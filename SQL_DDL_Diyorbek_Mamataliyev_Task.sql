-- Create the database and schema
CREATE DATABASE library_db;
CREATE SCHEMA library_schema;

-- Create the 'authors' table
CREATE TABLE library_schema.authors (
    author_id SERIAL PRIMARY KEY,
    author_name VARCHAR(100) NOT NULL,
    birthdate DATE,
    record_ts TIMESTAMP DEFAULT current_date,
    CHECK (birthdate >= '1900-01-01')  -- Ensure birthdate is valid
);

-- Create the 'books' table
CREATE TABLE library_schema.books (
    book_id SERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    author_id INT REFERENCES library_schema.authors(author_id),
    publication_date DATE,
    record_ts TIMESTAMP DEFAULT current_date,
    CHECK (publication_date >= '1900-01-01')  -- Ensure publication date is valid
);

-- Create the 'users' table
CREATE TABLE library_schema.users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    gender VARCHAR(10) CHECK (gender IN ('Male', 'Female', 'Other')),
    birthdate DATE,
    record_ts TIMESTAMP DEFAULT current_date,
    UNIQUE (username),
    CHECK (birthdate >= '1900-01-01')  -- Ensure birthdate is valid
);

-- Create the 'book_loans' table
CREATE TABLE library_schema.book_loans (
    loan_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES library_schema.users(user_id),
    book_id INT REFERENCES library_schema.books(book_id),
    loan_date DATE DEFAULT current_date,
    return_date DATE,
    record_ts TIMESTAMP DEFAULT current_date,
    CHECK (loan_date >= '2000-01-01'),  -- Ensure loan date is valid
    CHECK (return_date >= '2000-01-01'),  -- Ensure return date is valid
    CHECK (return_date >= loan_date)  -- Ensure return date is after loan date
);

-- Insert sample data into the tables
-- For simplicity, we will insert at least two rows into each table.
INSERT INTO library_schema.authors (author_name, birthdate) VALUES
    ('Arthur Conan Doyl', '1980-03-15'),
    ('Jonathan Swift', '1975-09-20');

INSERT INTO library_schema.books (title, author_id, publication_date) VALUES
    ('Sherlock Xolms', 1, '2005-05-10'),
    ('Gulliver', 1, '2010-12-03');

INSERT INTO library_schema.users (username, gender, birthdate) VALUES
    ('Diyorberk', 'Male', '2003-05-10'),
    ('Yusuf', 'Male', '2004-10-10');

INSERT INTO library_schema.book_loans (user_id, book_id, loan_date, return_date) VALUES
    (1, 1, '2023-10-15', '2023-11-15'),
    (2, 2, '2023-10-20', '2023-11-10');